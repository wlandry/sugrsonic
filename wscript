def configure(ctx):
    ctx.load("compiler_cxx gnu_dirs")

def options(ctx):
    ctx.load("compiler_cxx gnu_dirs")

def build(ctx):
    sources=['src/SugrSonic/SugrSonic.cxx',
             'src/SugrSonic/reset.cxx',
             'src/SugrSonic/update_u_ut.cxx',
             'src/SugrSonic/update_ux_uy.cxx']
    sugrsonic=ctx.shlib(
        source       = sources,
        target       = 'sugrsonic',
        # cxxflags     = ['-std=c++11','-g','-Wall','-Wextra','-msse4'],
        cxxflags     = ['-std=c++11','-Ofast','-Wall','-Wextra','-msse4'],
        )

    wave_sse_int16=ctx(
        features     = ['cxx','cprogram'],
        source       = ['test/wave.cxx'],
        target       = 'wave',
        # cxxflags     = ['-std=c++11','-g','-Wall','-Wextra','-msse4'],
        cxxflags     = ['-std=c++11','-Ofast','-Wall','-Wextra','-msse4'],
        use          = ['sugrsonic'],
        rpath        = [ctx.env.LIBDIR]
        )

    ctx.install_files(ctx.env.INCLUDEDIR,
                      'src/SugrSonic.hxx')
