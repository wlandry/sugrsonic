# Installation

To build, SugrSonic requires a C++11 compiler and Python > 2.7.
SugrSonic uses [waf](https://waf.io) (included with the source) to
build. This set of commands should work

    ./waf configure --prefix=$PWD
    ./waf
    ./waf install

That will create the testing binary

    bin/wave
