#include <fstream>
#include <iostream>
#include <string.h>
#include <chrono>
#include <cmath>

#include "../src/SugrSonic.hxx"

/// 

int main(int argc, char *argv[])
{
  if(argc<3 || argc>4)
    {
      std::cerr << "Usage: wave ITERATIONS OUTPUT_PREFIX OUTPUT_STEP\n"
                << "\n"
                << "Run a wave simulation of an initial gaussian impulse for\n"
                << "<ITERATIONS> iterations in a square with embedded walls.\n"
                << "Output is written to <OUTPUT_PREFIX>.<iteration>.tsv, \n"
                << "<OUTPUT_PREFIX>.visit.<iteration>, and "
                << "<OUTPUT_PREFIX>.visit.max\n";
      exit(-1);
    }

  const size_t NX(50*4);
  const size_t NY(50*4);

  const size_t step(argc!=4 ? 1 : std::stoi(argv[3]));
  if (step==0)
    {
      std::cerr << "Invalid step\n";
      exit(-1);
    }

  std::vector<int> u_t(NX*NY);
  std::vector<char> x_mask(NX*NY,1), y_mask(NX*NY,1);

  const int16_t sound(1 << 12);

  for(size_t x=0; x<NX; ++x)
    for(size_t y=0; y<NY; ++y)
      {
        /// Set up a gaussian impulse at the beginning.
        u_t[x + NX*y]=sound*std::exp(-((x-NX/2.)*(x-NX/2.) + (y-NY/2.)*(y-NY/2.))/(NX*NX/256.));

        /// Create two walls which reflect sound
        if((x>NX/6 && x<NX/4 && y>NY/4 && y<3*NY/4)
           || (x>NX/2 && x<3*NX/4 && y>NY/6 && y<NY/4))
          {
            x_mask[x + NX*y]=0;
            y_mask[x + NX*y]=0;
          }
      }

  sugrsonic::SugrSonic s(x_mask.data(), y_mask.data(), NX, NY);
  s.reset(u_t.data(), NX, NY);
  
  const size_t n_iterations(std::atoi(argv[1]));

  auto clock=std::chrono::high_resolution_clock();
  auto start(clock.now());
  s.step(n_iterations);
  auto finish(clock.now());
  std::chrono::duration<double> time_span
    = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start);
  std::cout << "time to run " << n_iterations << " timesteps: "
            << time_span.count() << "\n";

  /// Ideally, we would validate the output prefix, making sure that
  /// we are not overwriting /etc/password.  This program is only a
  /// simple example, not bulletproof production code.
  s.reset(u_t.data(), NX, NY);
  for(size_t iteration=0; iteration<n_iterations; ++iteration)
    {
      if (iteration%step==0)
        {
          {
            std::ofstream tsv_file(argv[2] + ("." + std::to_string(iteration) + ".tsv"));
            tsv_file << "x\ty\tu\n";
            for(size_t x=0; x<NX; ++x)
              for(size_t y=0; y<NY; ++y)
                tsv_file << x << "\t" << y << "\t"
                         << s.at(x,y) << "\n";
          }
          /// For opening in Visit.
          /// https://wci.llnl.gov/simulation/computer-codes/visit
          /// Open as PlainText.  Set the open option to 2D array.
          {
            std::ofstream visit_file(argv[2] + (".visit." + std::to_string(iteration)));
            for(size_t x=0; x<NX; ++x)
              {
                for(size_t y=0; y<NY; ++y)
                  visit_file << s.at(x,y)/(1.0*sound) << " ";
                visit_file << "\n";
              }
            visit_file << "\n";
          }
        }
      s.step(1);
    }
  {
    std::ofstream visit_file(argv[2] + std::string(".visit.max"));
    for(size_t x=0; x<NX; ++x)
      {
        for(size_t y=0; y<NY; ++y)
        visit_file << s.max_at(x,y)/(1.0*sound) << " ";
        visit_file << "\n";
      }
    visit_file << "\n";
  }
}
