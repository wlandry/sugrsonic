#include "../SugrSonic.hxx"

sugrsonic::SugrSonic::SugrSonic(const char x_mask[], const char y_mask[],
                                const size_t nx_in, const size_t ny_in):
  nx((nx_in + values_per_register - 1)/values_per_register), ny(ny_in), u(nx*ny)
{
  std::vector<int16_t> temp(values_per_register);
  for (size_t y=0; y<ny_in; ++y)
    for (size_t x=0; x<nx; ++x)
      {
        size_t offset=x*values_per_register + nx_in*y;

        std::fill(temp.begin(), temp.end(), 0xFFFF);
        for (size_t dx=offset; dx<(y+1)*nx_in && dx<offset+values_per_register;
             ++dx)
          temp[dx-offset]=(x_mask[offset] == 0 ? 0 : 0xFFFF);
        u[x + nx*y].x_mask=_mm_set_epi16(temp[7],temp[6],temp[5],temp[4],
                                         temp[3],temp[2],temp[1],temp[0]);

        std::fill(temp.begin(), temp.end(), 0xFFFF);
        for (size_t dx=offset; dx<(y+1)*nx_in && dx<offset+values_per_register;
             ++dx)
          temp[dx-offset]=(y_mask[offset] == 0 ? 0 : 0xFFFF);
        u[x + nx*y].y_mask=_mm_set_epi16(temp[7],temp[6],temp[5],temp[4],
                                         temp[3],temp[2],temp[1],temp[0]);
      }
}

