#include "../SugrSonic.hxx"
#include "divide_by_2.hxx"

namespace
{
  /// Return the value at a -dx offset by left shifting the current
  /// register by one value and or'ing it with the previous register
  /// shifted right.

  /// Values at dy offsets do not require any of this, because values
  /// are packed in x, but not y.
  inline __m128i minus_offset(const __m128i &center, const __m128i &minus)
  {
    __m128i shift=_mm_slli_si128(center,sizeof(int16_t));
    __m128i shift_minus=
      _mm_srli_si128(minus,(sugrsonic::values_per_register - 1)*sizeof(int16_t));
    return _mm_or_si128(shift,shift_minus);
  }

  /// Create an updated version of u_x using the equation
  ///
  /// u_x(t+dt,x,y) = u_x(t-dt,x,y) + (u_t(t,x+dx,y) - u_t(t,x-dx,y))/2
  /// 
  /// Breaking down the parts:
  ///
  /// _mm_sub_epi16(t,minus)  -> u_t(t,x+dx,y) - u_t(t,x-dx,y)
  /// divide_by_2(...)        -> (u_t(t,x+dx,y) - u_t(t,x-dx,y))/2
  /// _mm_add_epi16(x,...)    -> u_x(t-dt) + (u_t(t,x+dx,y) - u_t(t,x-dx,y))/2
  /// _mm_and_si128(...,mask) -> apply boundary conditions
  ///
  /// The main difference between new_x and new_y is that computing
  /// u_t(t,x-dx,y) is more involved than computing u_t(t,x,y-dy)
  inline __m128i new_x(const __m128i &t, const __m128i &t_minus,
                       const __m128i &x, const __m128i &mask)
  {
    __m128i minus(minus_offset(t,t_minus));
    return
      _mm_and_si128(_mm_add_epi16(x,sugrsonic::divide_by_2(_mm_sub_epi16
                                                           (t,minus))),mask);
  }

  inline __m128i new_y(const __m128i &t, const __m128i &t_minus,
                       const __m128i &y, const __m128i &mask)
  {
    return
      _mm_and_si128(_mm_add_epi16(y,sugrsonic::divide_by_2(_mm_sub_epi16
                                                           (t,t_minus))),mask);
  }
}

void sugrsonic::SugrSonic::update_ux_uy()
{
  U *center=u.data();
  U *x_minus=center-1;
  U *y_minus=center-nx;

  /// x=0, y=0
  center->x=new_x(center->t,_mm_setzero_si128(),center->x,center->x_mask);
  // center->y=new_y(center->t,_mm_setzero_si128(),center->y,center->y_mask);
  ++center;
  ++x_minus;
  ++y_minus;

  /// y=0
  for (size_t x=1; x<nx; ++x)
    {
      center->x=new_x(center->t, x_minus->t, center->x, center->x_mask);
      // center->y=new_y(center->t, _mm_setzero_si128(), center->y, center->y_mask);
      ++center;
      ++x_minus;
      ++y_minus;
    }

  for (size_t y=1; y<ny; ++y)
    {
      /// x=0
      center->x=new_x(center->t, _mm_setzero_si128(), center->x, center->x_mask);
      center->y=new_y(center->t, y_minus->t, center->y, center->y_mask);
      ++center;
      ++x_minus;
      ++y_minus;
      for (size_t x=1; x<nx; ++x)
        {
          center->x=new_x(center->t, x_minus->t, center->x, center->x_mask);
          center->y=new_y(center->t, y_minus->t, center->y, center->y_mask);
          ++center;
          ++x_minus;
          ++y_minus;
        }
    }
}

