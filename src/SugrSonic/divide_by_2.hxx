#pragma once

#include <smmintrin.h>

namespace sugrsonic
{
inline __m128i divide_by_2(const __m128i &in)
{
  /// We have to do a little song and dance because right shifting -1
  /// gives you -1, not zero.  So we take the absolute value first,
  /// shift it, then fix the sign.
  return _mm_sign_epi16(_mm_srai_epi16(_mm_abs_epi16(in),1),in);
}
}
