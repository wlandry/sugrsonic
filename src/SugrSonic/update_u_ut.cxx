#include "../SugrSonic.hxx"
#include "divide_by_2.hxx"

namespace
{
  /// Return the value at a +dx offset by right shifting the current
  /// register by one value and or'ing it with the previous register
  /// shifted left.

  /// Values at dy offsets do not require any of this, because values
  /// are packed in x, but not y.
  inline __m128i plus_offset(const __m128i &plus, const __m128i &center)
  {
    __m128i shift_plus=
      _mm_slli_si128(plus,(sugrsonic::values_per_register - 1)*sizeof(int16_t));
    __m128i shift=_mm_srli_si128(center,sizeof(int16_t));
    return _mm_or_si128(shift_plus,shift);
  }

  /// Create an updated version of u_t using the equation
  ///
  /// u_t(t+dt,x,y) = u_t(t-dt,x,y) + (u_x(t,x+dx,y) - u_x(t,x-dx,y)
  ///                                  + u_y(t,x,y+dy) - u_y(t,x,y-dy))/2
  ///
  /// Breaking down the parts
  ///
  /// _mm_sub_epi16(plus,x)     -> u_x(t,x+dx,y) - u_x(t,x-dx,y)
  /// _mm_sub_epi16(y_plus,y)   -> u_y(t,x,y+dy) - u_y(t,x,y-dy)
  /// divide_by_2(...)          -> (u_x(t,x+dx,y) - u_x(t,x-dx,y)
  ///                               + u_y(t,x,y+dy) - u_y(t,x,y-dy))/2
  /// _mm_add_epi16(t,...)      -> u_t(t-dt,x,y)
  ///                                + (u_x(t,x+dx,y) - u_x(t,x-dx,y)
  ///                                   + u_y(t,x,y+dy) - u_y(t,x,y-dy))/2
  inline __m128i new_t(const __m128i &x_plus, const __m128i &x,
                       const __m128i &y_plus, const __m128i &y,
                       const __m128i &t)
  {
    __m128i plus(plus_offset(x_plus,x));
    return _mm_add_epi16(t,sugrsonic::divide_by_2(_mm_add_epi16
                                                  (_mm_sub_epi16(plus,x),
                                                   _mm_sub_epi16(y_plus,y))));
  }

  /// u(t+dt,x,y) = u(t-dt,x,y) + u_t(t,x,y)/2
  inline __m128i new_u(const __m128i &t, const __m128i &u)
  {
    return _mm_add_epi16(u,sugrsonic::divide_by_2(t));
  }
}

void sugrsonic::SugrSonic::update_u_ut()
{
  U *center=u.data();
  U *x_plus=center+1;
  U *y_plus=center+nx;

  for (size_t y=0; y<ny-1; ++y)
    {
      for (size_t x=0; x<nx-1; ++x)
        {
          center->t=new_t(x_plus->x, center->x, y_plus->y, center->y, center->t);
          center->u=new_u(center->t,center->u);
          center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
          ++center;
          ++x_plus;
          ++y_plus;
        }
      /// x=nx-1
      center->t=new_t(_mm_setzero_si128(), center->x, y_plus->y, center->y,
                      center->t);
      center->u=new_u(center->t,center->u);
      center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
      ++center;
      ++x_plus;
      ++y_plus;
    }
  /// y=ny-1
  for (size_t x=0; x<nx-1; ++x)
    {
      center->t=new_t(x_plus->x, center->x, _mm_setzero_si128(), center->y,
                      center->t);
      center->u=new_u(center->t,center->u);
      center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
      ++center;
      ++x_plus;
      ++y_plus;
    }
  /// x=nx-1, y=ny-1
  center->t=new_t(_mm_setzero_si128(), center->x, _mm_setzero_si128(),
                  center->y, center->t);
  center->u=new_u(center->t,center->u);
  center->u_max=_mm_max_epi16(center->u_max,_mm_abs_epi16(center->u));
  ++center;
  ++x_plus;
  ++y_plus;
}
